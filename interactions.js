$(document).ready(function() {

    $("#taskform").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: './session.php',
            data: $(this).serialize(),
            success: function(result) {
                //  console.log(response);
                var jsonData = JSON.parse(result);
                console.log(JSON.parse(result));
                alert("Task has been succesfully inserted!");
                var task = "";
                $(jsonData).each(function(index, value) {
                    task += '<li>' + value + '</li>'
                });
                $("#task").html(task);


            }
        })
    })

    $("#clearSession").on('click', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'clearsession.php',
            success: function(response) {
                var jsonData = JSON.parse(response)
                console.log(jsonData);
            }
        })
    })
})